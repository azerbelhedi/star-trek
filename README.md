# Start Trek Simulator: Your trek into the stars

### All purpose kinematics/dynamics simulator

- Current version V0.2
- Check all versions release notes [here](https://gitlab.com/azerbelhedi/star-trek/-/blob/master/CHANGELOG.md)
- This library is still under construction.
- The goal is to make it as easy as possible to simulate a rocket trajectory.
- The library could also be used for non rocket flight simulation purposes, like simulating a ball falling from  
  the sky or anything that moves following newtonian mechanics.

### User guide

- If you are planning to use this library to run your own simulation, than this section is for you.

#### Part 1: Setup

#### Step 1.0: clone library

- Go to any directory where you want to clone the library to use it.
- Open terminal in that directory.
- Run `git clone https://gitlab.com/azerbelhedi/star-trek`
- Make sure that you are in the "master branch" by running `git checkout master`
- Run `cd starterk` to enter the library directory where you can run your simulations
- Note: All commands you re going to use to work with this simulator should be run from "startrek/" directory.
  Otherwise it won't work. So always make sure you that you are in the right directory.

#### Step 1.1: compile

- Run `make compile`
- This command will compile your main.cpp which is already provided, as well as all needed modules.
- If the code contains no erros, an executable "main.exe" file will be generated, which will be called to start  
  the simulation.

#### Step 1.2: run

- Run `make run`
- As mentioend in step 1.1 this command will start the simulation.
- If it goes well with no run-time errors, the simulation results will be saved in a ".dat" file  
  under "/startrek/apps/ball/visualize/".
- Some "gnuplot" script files will be genearated too. It will be used in the next step to plot the results  
  from the ".dat" file into a ".png" picture.

#### Step 1.3: plot results

- Run `make plot`
- This command will use gnuplot to generate ".png" files showing all the variables that you decided to plot  
  in the "main.cpp" file.
- It will aslo use "gthumb" to open the photos automatically once they are generated.
- gthumb is a program in linux that opens photos. If you don't have it installed or have a different OS,  
  you can open the photos manually from their
  directory "startrek/apps/ball/visualize".

#### Step 1.4: clean

- Run `make clean`
- This command will remove all files that were generated in run-time, like the ".png", ".dat"  
  and "gnuplot" scripts.
- It is recommanded to clean the directory using this command every time you wanna compile and/or  
  run the simulation again.
- If the clean command runs without problems, the terminal screen will be cleaned.  
  Otherwise you'll be able to read the error message(s).

#### Compile then run then plot

- Run `make all` or `make`
- Run this command if you want to compile, run, and plot all at once.

#### Part 2: Simulating your first model

- This part is yet to be developed.

### Developer guide

- If you'd like to contribute to this librarby by introducing more concepts/components or improve the existing once,  
  than this section is for you.
- Unfortunately this library is still a couple of days/ weeks old and still seeing a lot of radical changes.  
  Therefore the whole structure of the code is not stable yet and will
  continue to see more big changes in the coming couple of weeks.
- Once the code reaches a relatively stable state, I will share a detailed explanation of how all components work  
  and how they interact with each other.
- For any more details feel free to reach me at "benelhedi.azer@gmail.com".
- I'd like to hear about what I did wrong and how I can fix it ;)
