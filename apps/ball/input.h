#pragma once
#include <inputType.h>

namespace InputData {
using namespace PhysicalVariables;

Input freeFallBall = {

    {position, XYZ(0, 5000, 0)},
    {velocity, XYZ(0, 0, 0)},
    {acceleration, XYZ(0, 0, 0)},
    {force, XYZ(0, 0, 0)},
    {mass, XYZ(0.2)},
    {dragCof, XYZ(0.5)},
    {area, XYZ(0.05)}

};

Input thrownBall = {

    {position, XYZ(0, 0, 0)},
    {velocity, XYZ(0, 0, 0)},
    {acceleration, XYZ(0, 0, 0)},
    {force, XYZ(1000, 1000, 0)},
    {mass, XYZ(0.2)},
    {dragCof, XYZ(0.5)},
    {area, XYZ(0.05)}

};
}  // namespace InputData
