#pragma once

#include <drag.h>
#include <environment.h>
#include <gravity.h>
#include <inputType.h>
#include <object.h>
#include <plot.h>

#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <vector>

Gravity* gravity = new Gravity();
Drag* drag = new Drag();