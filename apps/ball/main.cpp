#include <includes.h>
#include <input.h>

using namespace std;

bool Object::stopCondition(Input environmentProperties) {
  if (properties[position].y <= -0.0001) return true;
  return false;
}

int main() {
  Environment earth;

  Object* ball = new Object("steal-ball", InputData::thrownBall);

  ball->consider({gravity, drag});

  earth.addObject(ball);

  earth.run(100, 31);

  ball->plot({XY, TX, TY, TVx, TVy, VxVy, TAx, TAy, TFx, TFy});

  // ball->plot({position, velocity, acceleration, force, gravity, drag});

  return 0;
}
