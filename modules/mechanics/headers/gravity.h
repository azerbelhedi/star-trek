#pragma once
#include <force.h>

class Gravity : public Force {
 public:
  Gravity();
  void compute(Input objectProperties, Input environmentProperties);
};
