#pragma once

#include <XYZ.h>
#include <force.h>

class Drag : public Force {
 public:
  Drag();
  void compute(Input objectProperties, Input environmentProperties);
};
