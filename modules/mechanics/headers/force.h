#pragma once
#include <XYZ.h>
#include <inputType.h>

/**
 * value: mathematical 3D vector of the force (not normed)
 * poa: point of application of the force
 */

class Force {
 public:
  XYZ value, poa;

  Force(XYZ value, XYZ poa);

  // new methods to implement
  virtual void compute(Input objectProperties, Input environmentProperties);
};