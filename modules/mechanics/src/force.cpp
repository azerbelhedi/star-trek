#include <XYZ.h>
#include <force.h>
#include <inputType.h>

#include <iostream>

using namespace std;

Force::Force(XYZ value, XYZ poa) : value(value), poa(poa) {}

void Force::compute(Input objectProperties, Input environmentProperties) {}
