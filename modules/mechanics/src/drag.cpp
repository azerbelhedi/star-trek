#include <drag.h>

Drag::Drag() : Force(XYZ(0, 0, 0), XYZ(0, 0, 0)) {}

void Drag::compute(Input objectProperties, Input environmentProperties) {
  value = (-objectProperties[velocity].sign() * environmentProperties[ro] *
           objectProperties[velocity] * objectProperties[velocity] *
           objectProperties[dragCof] * objectProperties[area]) /
          2;
}