#include <gravity.h>

Gravity::Gravity() : Force(XYZ(0, 0, 0), XYZ(0, 0, 0)) {}

void Gravity::compute(Input objectProperties, Input environmentProperties) {
  value = -objectProperties[mass] * environmentProperties[gravityConstant];
}