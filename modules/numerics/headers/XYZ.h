#pragma once
#include <iostream>
#include <vector>

using namespace std;

class XYZ {
 public:
  double x, y, z;

  XYZ();
  XYZ(double value);
  XYZ(double x, double y, double z);
  XYZ operator+(XYZ a);
  XYZ& operator=(vector<double> values);
  XYZ& operator=(double value);
  XYZ operator*(XYZ a);
  XYZ operator/(XYZ a);
  XYZ operator-();
  XYZ operator-(XYZ a);
  XYZ operator+=(XYZ a);
  XYZ sign();
  friend std::ostream& operator<<(std::ostream& os, const XYZ& a);
};