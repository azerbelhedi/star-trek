#include <XYZ.h>

#include <iostream>
#include <vector>

using namespace std;

XYZ::XYZ() {}

XYZ::XYZ(double value) : x(value), y(value), z(value) {}

XYZ& XYZ::operator=(vector<double> values) {
  if (values.size() != 3) {
    cout << "invalid Cartesian assignment\n";
    // next line is just triggering an error
    values[values.size() + 1] = 0;
    return *this;
  }

  x = values[0];
  y = values[1];
  z = values[2];

  return *this;
}

XYZ& XYZ::operator=(double value) {
  x = y = z = value;
  return *this;
}

XYZ::XYZ(double x, double y, double z) : x(x), y(y), z(z) {}

XYZ XYZ::operator+(XYZ a) {
  XYZ r(0, 0, 0);
  r.x = a.x + x;
  r.y = a.y + y;
  r.z = a.z + z;
  return r;
}

std::ostream& operator<<(std::ostream& os, const XYZ& a) {
  os << "x: " << a.x << "\ty: " << a.y << "\tz: " << a.z << "\n";
  return os;
}

XYZ XYZ::operator*(XYZ a) {
  XYZ r;
  r.x = x * a.x;
  r.y = y * a.y;
  r.z = z * a.z;

  return r;
}

XYZ XYZ::operator/(XYZ a) {
  XYZ r;
  r.x = x / a.x;
  r.y = y / a.y;
  r.z = z / a.z;

  return r;
}

XYZ XYZ::operator-() { return *this * (-1); }

XYZ XYZ::operator-(XYZ a) {
  XYZ r;
  r.x = x - a.x;
  r.y = y - a.y;
  r.z = z - a.z;

  return r;
}

XYZ XYZ::operator+=(XYZ a) {
  XYZ r(0, 0, 0);
  r = a + *this;
  return r;
}

XYZ XYZ::sign() {
  XYZ r(0);

  r.x = x >= 0 ? 1 : -1;
  r.y = y >= 0 ? 1 : -1;
  r.z = z >= 0 ? 1 : -1;

  return r;
}