#pragma once
#include <iostream>
#include <map>

class MathParser
{

private:
    static std::map<std::string, double *> variablesProvider;
    bool _isLeaf();
    std::string _expression;
    MathParser *_left, *_right;

public:
    MathParser(std::string expression);
    void decompose(char op);
    double compute();
    double functionsProvider(std::string, double, double);
    static void registerVariables(std::map<std::string, double *>);
};