#pragma once
#include <XYZ.h>
#include <force.h>
#include <inputType.h>
#include <observable.h>

/**
 * CM: center of mass
 * CP: center of pressure
 * MIx: momnet of inertia around X
 * MIy: moment of inertia around Y
 * MIz: moment of inertia around Z
 * forces: array (vector) of forces applied on that object
 */

class Object : public Observable {
 public:
  Input properties;
  vector<Force *> forces;

  Object(string name, Input input);

  virtual void preCompute(int frameNumber, double time);
  void compute(double framesRate);
  virtual void postCompute();
  // virtual XYZ computeForce(Input properties);

  // new methos to implement
  void consider(vector<Force *> newForces);
  void computeForces(Input environmentProperties);
  bool stopCondition(Input environmentProperties);
};