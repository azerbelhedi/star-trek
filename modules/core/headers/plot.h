#pragma once

#include <string>
#include <vector>
using namespace std;

enum plotValue {
  // position
  TX,
  TY,
  TZ,
  XY,
  // velocity
  TVx,
  TVy,
  TVz,
  VxVy,
  // acceleration
  TAx,
  TAy,
  TAz,
  AxAy,
  // force
  TFx,
  TFy,
  TFz,
  FxFy,
};

class Plot {
 public:
  Plot();
  static vector<string> getValue(plotValue p);
};