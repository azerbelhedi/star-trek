#pragma once
#include <XYZ.h>
#include <force.h>
#include <inputType.h>
#include <object.h>

#include <iostream>
#include <vector>

using namespace std;

class Environment {
 public:
  Input properties = {

      {gravityConstant, XYZ(0, 9.81, 0)},
      {pressure, XYZ(10000)},
      {ro, XYZ(1.225)},
      {simulationTime, XYZ(0)}

  };

  vector<Object *> objects;

  Environment();
  void run(double framesRate, double duration);
  void addObject(Object *object);
  Force computeForce();
};
