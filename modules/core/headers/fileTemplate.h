#pragma once
#include <plot.h>

#include <iostream>
#include <vector>

using namespace std;

class FileTemplate {
 public:
  FileTemplate();
  void createGnuPlotFile(string name, string title, string plotCommand);
  void cretateMakePlotFile(string name, vector<plotValue> plots);
};
