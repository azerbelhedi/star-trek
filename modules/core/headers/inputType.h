#pragma once
#include <XYZ.h>

#include <map>
#include <vector>
using namespace std;

namespace PhysicalVariables {
enum Variable {
  position,
  velocity,
  acceleration,
  force,
  mass,
  dragCof,
  area,
  gravityConstant,
  pressure,
  ro,
  simulationTime
};
};  // namespace PhysicalVariables

using namespace PhysicalVariables;

typedef map<PhysicalVariables::Variable, XYZ> Input;
