#include <XYZ.h>
#include <force.h>
#include <inputType.h>
#include <object.h>

#include <iostream>

using namespace std;

Object::Object(string name, Input input)
    : Observable(name, {"t", "x", "y", "z", "v_x", "v_y", "v_z", "a_x", "a_y",
                        "a_z", "f_x", "f_y", "f_z"}),
      properties(input) {}

void Object::preCompute(int frameNumber, double time) {}

void Object::compute(double framesRate) {
  properties[acceleration] = properties[force] / properties[mass];
  properties[velocity] =
      properties[velocity] + properties[acceleration] / framesRate;
  properties[position] =
      properties[position] + properties[velocity] / framesRate;
}

void Object::postCompute() {}

void Object::consider(vector<Force *> newForces) {
  for (vector<Force *>::iterator f = begin(newForces); f != end(newForces);
       f++) {
    forces.push_back(*f);
  }
}

void Object::computeForces(Input environmentProperties) {
  properties[force] = 0;
  for (vector<Force *>::iterator f = begin(forces); f != end(forces); f++) {
    (*f)->compute(properties, environmentProperties);
    properties[force] = properties[force] + (*f)->value;
  }
}
