#include <fileTemplate.h>

#include <fstream>

FileTemplate::FileTemplate(){};

void FileTemplate::createGnuPlotFile(string name, string title,
                                     string plotCommand) {
  ofstream file;
  file.open("visualize/" + name + "_" + title + ".p");

  file << "set terminal png size 1000,1000\n";
  file << "set output '" << name << "_" << title << ".png'\n";
  file << "set title '" << name << "_" << title << "'\n";
  file << "set grid\n";
  file << plotCommand;
}

void FileTemplate::cretateMakePlotFile(string name, vector<plotValue> plots) {
  ofstream file;
  file.open("visualize/makefile");
  file << "plot:\n";
  for (vector<plotValue>::iterator p = plots.begin(); p != plots.end(); p++) {
    file << "\tgnuplot " << name << "_" << Plot::getValue(*p)[0] << ".p\n";
  }
  file << "\tgthumb ";
  for (vector<plotValue>::iterator p = plots.begin(); p != plots.end(); p++) {
    file << name << "_" << Plot::getValue(*p)[0] << ".png ";
  }
  file << "&\n";

  file << "clean:\n\trm ";
  for (vector<plotValue>::iterator p = plots.begin(); p != plots.end(); p++) {
    file << name << "_" << Plot::getValue(*p)[0] << ".png " << name << "_"
         << Plot::getValue(*p)[0] << ".p ";
  }
  file << "&& clear\n";
  file << "\trm " << name << ".dat " << name << ".csv ";
  file << "&& clear";
}