#include <environment.h>
#include <object.h>

Environment::Environment() {}

void Environment::addObject(Object *object) { objects.push_back(object); }

void Environment::run(double framesRate, double duration) {
  bool endSimulation = false;
  for (int i = 0; i < duration * framesRate; i++) {
    properties[simulationTime] = properties[simulationTime] + (i / framesRate);
    if (endSimulation) break;
    for (vector<Object *>::iterator ob = begin(objects); ob != end(objects);
         ob++) {
      endSimulation = (*ob)->stopCondition(properties);
      (*ob)->preCompute(i, i / framesRate);
      vector<double> variables;
      XYZ p = (*ob)->properties[position];
      XYZ v = (*ob)->properties[velocity];
      XYZ a = (*ob)->properties[acceleration];
      XYZ f = (*ob)->properties[force];

      variables = {

          i / framesRate,
          p.x,
          p.y,
          p.z,
          v.x,
          v.y,
          v.z,
          a.x,
          a.y,
          a.z,
          f.x,
          f.y,
          f.z

      };

      (*ob)->updateVariables(variables);
      (*ob)->compute(framesRate);
      // Important: force should be computed last. Otherwise initial force won't
      // produce acceleration and initial speed.
      (*ob)->computeForces(properties);
      (*ob)->postCompute();
    }
  }
}
