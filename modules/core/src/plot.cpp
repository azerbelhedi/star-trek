#include <plot.h>

Plot::Plot() {}

vector<string> Plot::getValue(plotValue plotValue) {
  switch (plotValue) {
    // position
    case TX:
      return {"t_x", "1:2"};
      break;
    case TY:
      return {"t_y", "1:3"};
      break;
    case TZ:
      return {"t_z", "1:4"};
      break;
    case XY:
      return {"x_y", "2:3"};
      break;

    // velocity
    case TVx:
      return {"t_vx", "1:5"};
      break;
    case TVy:
      return {"t_vy", "1:6"};
      break;
    case TVz:
      return {"t_vz", "1:7"};
      break;
    case VxVy:
      return {"vx_vy", "5:6"};
      break;

      // acceleration
    case TAx:
      return {"t_ax", "1:8"};
      break;
    case TAy:
      return {"t_ay", "1:9"};
      break;
    case TAz:
      return {"t_az", "1:10"};
      break;
    case AxAy:
      return {"ax_ay", "8:9"};
      break;

      // force
    case TFx:
      return {"t_fx", "1:11"};
      break;
    case TFy:
      return {"t_fy", "1:12"};
      break;
    case TFz:
      return {"t_fz", "1:13"};
      break;
    case FxFy:
      return {"fx_fy", "11:12"};
      break;

    default:
      return {"plot_error", "0:0"};
      break;
  }
}