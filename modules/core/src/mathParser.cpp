#include <mathParser.h>
#include <vector>
#include <algorithm>
#include <stdexcept>

MathParser::MathParser(std::string expression) : _expression(expression)
{
    // std::cout << _expression << "\n";
    decompose('+');
    decompose('-');
    decompose('*');
    decompose('/');
}

void MathParser::registerVariables(std::map<std::string, double *> variablesMap)
{
    MathParser::variablesProvider = variablesMap;
}

// creating instance of variablesProvider map (static member)
std::map<std::string, double *> MathParser::variablesProvider;

void MathParser::decompose(char op)
{
    // remove all blanck spaces
    _expression.erase(remove(_expression.begin(), _expression.end(), ' '), _expression.end());

    for (int i = 0; i < _expression.length(); i++)
    {
        if (_expression.at(i) == op)
        {
            _left = new MathParser(_expression.substr(0, i));
            _right = new MathParser(_expression.substr(i + 1, _expression.length() - i + 1));
            _expression = _expression.at(i);
            break;
        }
    }
}

double MathParser::compute()
{
    if (!_isLeaf())
    {
        return functionsProvider(_expression, _left->compute(), _right->compute());
    }
    // std::cout << variablesProvider.count(_expression) ? *variablesProvider[_expression] : std::stod(_expression);
    return variablesProvider.count(_expression) ? *variablesProvider[_expression] : std::stod(_expression);
}

double MathParser::functionsProvider(std::string op, double a, double b)
{
    if (op == "+")
        return a + b;
    if (op == "-")
        return a - b;
    if (op == "*")
        return a * b;
    if (op == "/")
        return a / b;
    throw std::invalid_argument("operation '" + op + "' is not supported by the MathParser");
}

bool MathParser::_isLeaf()
{
    return !_left && !_right;
}