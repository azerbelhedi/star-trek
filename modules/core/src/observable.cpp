#include <fileTemplate.h>
#include <observable.h>

#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>

using namespace std;

Observable::Observable(string name, vector<string> variablesNames)
    : name(name), variablesNames(variablesNames) {
  for (auto i = variablesNames.begin(); i != variablesNames.end(); i++) {
    variables.push_back({});
  }
}

void Observable::updateVariables(vector<double> newVariables) {
  for (int i = 0; i < newVariables.size(); i++) {
    variables[i].push_back(newVariables[i]);
  }
}

void Observable::plot(vector<plotValue> plots) {
  ofstream datFile, csvFile;
  datFile.open("visualize/" + name + ".dat");
  csvFile.open("visualize/" + name + ".csv");

  datFile << "# " << name << "\n# ";
  for (vector<string>::iterator name = variablesNames.begin();
       name != variablesNames.end(); name++) {
    datFile << *name << setw(12);
  }
  for (vector<string>::iterator name = variablesNames.begin();
       name != variablesNames.end(); name++) {
    csvFile << *name << ";";
  }

  datFile << "\n";
  csvFile << "\n";

  for (int frame = 0; frame < variables[0].size(); frame++) {
    datFile << " " << left;
    for (int v = 0; v < variables.size(); v++) {
      datFile << setw(12) << variables[v][frame];
    }
    datFile << "\n";
  }

  for (int frame = 0; frame < variables[0].size(); frame++) {
    for (int v = 0; v < variables.size(); v++) {
      csvFile << variables[v][frame] << ";";
    }
    csvFile << "\n";
  }

  FileTemplate fileTemplate;
  for (vector<plotValue>::iterator p = plots.begin(); p != plots.end(); p++) {
    string plotVariables = "";

    string plotCommand = "plot 'steal-ball.dat' using " +
                         Plot::getValue(*p)[1] + " title '" +
                         Plot::getValue(*p)[0] + "' with lines";

    fileTemplate.createGnuPlotFile(name, Plot::getValue(*p)[0], plotCommand);
  }
  fileTemplate.cretateMakePlotFile(name, plots);
}