# Change log

## v0.2 31.05.2022

### New features and important changes

#### Force class

- Very different and more capable than the old Force class
- Force class is inherited to create spesific forces like gravity
  and drag.
- All needed forces are then passed to the object constructor.
- All passed forces will be used in every simulation frame to calculate
  total force of the respective object.
- The old Force class is removed

#### Object.consider(forces)

- Pre- and by user defined forces are just passed to objects through  
  object.consider(vector<force \*> forces).
- The simulator will take care of all the rest, computing forces,  
  adding them and calculating the other properties respectively.
- NOTE: every force could be passed to many objects as the same gravity  
  for example acts on all objects the same way. But this is yet to be tested.

#### Input class

- All classes that are part of the simulation model do receive an Input type variable
  through the contructor that describes all the phyisical properites of the component
- Input variables are defined in a seperate "input.h" file because I think that's a clean way to do it.
- This should be easier for users with no CPP experience to edit the model variables.

#### CSV file output

- Now, all the simulation data is saved on both .dat and .csv file in the hope that the .csv file will
  be easily used by paraview to visulize the data with more freedom compared to PNGs.

#### Simulation stop condition

- Users can override "bool Object::stopCondition(Input)" to end the simulation under any conditions they want.
- For example when the object hits the ground => object[position].y == 0.

#### Main file reduction

- Users can now run a simple simulation with just 5 lines of code. All inside the main file.

### Future features notes

- Simulate object's spin
- Objects constructor input check and error throwing.
- Support CSV output to visualize data using paraView.
- Add numerical interpolation tool
- Make plot funntion easier by making plot name more compact and readable.
  like "postion" instead of (XY, TX, TY, TZ)
- Adding the ability to "trace" and plot any variable over any variable.  
  Even user defined variables.
- Mathematical parser that will enable users to write force's equation as a string  
  instead of overriding the "force.compute()" function.
  As a result no inherited force classes will be needed unless the force description  
  is too complicated to be handled by the math-parser.
- Less classes means better performance.

### Far-fetched futures features:

- use openFoam as a third party to simulate drag force over angle of attack of objects of any shape.

## v0.1 23.05.2022

- Very basic prototype and a very long main file that's mostly not readable.
