///////  Tests  ///////

#include <parserTest.h>
#include <object.h>

int main(){
    std::cout << "runnig tests:" << "\n";
    //treeTest();
    parserTest();
}

// this is code should be included here to pass compilation.
// This is caused by a poor design decision concerning the stopCondition feature.
// Should be fixed in the future. 
bool Object::stopCondition(Input environmentProperties) {
  if (properties[position].y <= -0.0001) return true;
  return false;
}