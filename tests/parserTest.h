#pragma once
#include <iostream>
#include <mathParser.h>

typedef std::map<std::string, double> TestsMap;

bool parserUnitTest(int testID, std::string expression, double expectedResult);

void parserTest()
{
    std::cout << "\nstart parser test:\n\n";

    double f = 5, m = 3, g = 10, k = 2, four = 4;
    std::map<std::string, double *> provider{
        {"f", &f},
        {"m", &m},
        {"g", &g},
        {"k", &k},
        {"four", &four}};
    MathParser::registerVariables(provider);

    TestsMap tests{
        {"f + m + g - k", 16},
        {"f + m + g - k + 4", 20},
        {"f + 4", 9},
        {"g - f + 4", 9},
        {"g - f + four", 9},

        {"g*f", 50},
        {"g*f*3", 150},
        {"g*f*3 - k", 148},
        {"g + f * k", 20},
        {"g + f * k - 4", 16},
        {"g + f * k - 4*four", 4},

        {"g/f", g/f},
        {"g/f * k", g/f * k},
        {"m * g/f * k", m * g/f * k},
        {"m*g-k/four+4-3*5/k+50-2/m", m*g-k/four+4-3*5/k+50-2/m},
    };

    int index = 0;
    for (auto const &[expression, result] : tests)
    {
        parserUnitTest(index++, expression, result);
    }

    std::cout << "\nend parser test:\n";
}

bool parserUnitTest(int testID, std::string expression, double expectedResult)
{
    MathParser parser(expression);
    bool result = parser.compute() == expectedResult;
    std::cout << "test " << testID << ": " << (result ? "success" : "failure : " + expression) << "\n";
    return result;
}